#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D2D::NoInteresection;
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
    return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    bool intersection = false;
    Vector3d NormalThirdPlane = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));

    if(NormalThirdPlane.squaredNorm()<=toleranceParallelism*toleranceParallelism)
    {
        if(abs(rightHandSide(0)-rightHandSide(1))<=toleranceIntersection)
        {
            intersectionType = Coplanar;
            intersection = false;
        } else {
            intersectionType = NoInteresection;
            intersection = false;
        }
    } else {
        rightHandSide(2) = 0; //vincolo che piano passi per origine
        intersectionType = LineIntersection;
        intersection = true;
        tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
        Matrix3d solverMatrix;
        solverMatrix = matrixNomalVector.inverse();
        pointLine = solverMatrix * rightHandSide;
    }

    return intersection;
}
