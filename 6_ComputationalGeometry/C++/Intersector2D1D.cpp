#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoInteresection;
    intersectionParametricCoordinate = 0.0;

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
    return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
    return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    bool intersection = false;
    double num = (*planeTranslationPointer) - (*planeNormalPointer).dot(*lineOriginPointer);
    double den = (*planeNormalPointer).dot(*lineTangentPointer);

    if(abs(den)<=toleranceParallelism)
    {
        if(abs(num)<=toleranceIntersection)
        {
            intersectionType = Coplanar;
            intersection = false;
        } else {
            intersectionType = NoInteresection;
            intersection = false;
        }
    } else {
        intersectionType = PointIntersection;
        intersection = true;
        intersectionParametricCoordinate = num/den;
        intersectionPoint = (*lineOriginPointer) + intersectionParametricCoordinate * (*lineTangentPointer);
    }

    return intersection;
}
