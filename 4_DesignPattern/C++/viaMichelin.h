#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
    private:
      string _busFilePath;
      vector<Bus> _buses;  //vettore in cui ogni elemento è una istanza della classe Bus
      int _numBuses;

    public:
      BusStation(const string& busFilePath) { _busFilePath = busFilePath; }

      void Load();
      int NumberBuses() const  { return _numBuses; }
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {
    private:
      string _mapFilePath;
      int _numberRoutes;
      int _numberStreets;
      int _numberBusStops;
      vector<Route> _routes;
      vector<Street> _streets;
      vector<BusStop> _busStops;
      vector<int> _streetsFrom;
      vector<int> _streetsTo;
      vector<vector<int>> _routeStreets;   //vettore di vettori di int
      void Reset();
    public:
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath; }
      void Load();
      int NumberRoutes() const { return _numberRoutes; }
      int NumberStreets() const { return _numberStreets; }
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;

    private:
      const IMapData& _mapData;
      const IBusStation& _busStation;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) : _mapData(mapData), _busStation(busStation) { }

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
    private:
      const IMapData& _mapData;

    public:
      MapViewer(const IMapData& mapData) : _mapData(mapData)  { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
