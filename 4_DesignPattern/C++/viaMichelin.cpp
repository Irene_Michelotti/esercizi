# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    //resetta _buses e _numBuses
    _numBuses = 0;
    _buses.clear();

    //apre file
    ifstream file;
    file.open(_busFilePath.c_str());

    //controlla che file sia stato aperto correttamente
    if (file.fail())
        throw runtime_error("Something goes wrong");

    //load dei bus
    try {
        string line;
        getline(file, line);  //salta il commento (prima riga)
        getline(file, line);
        istringstream convertN;
        convertN.str(line);
        convertN >> _numBuses;

        getline(file, line);  //salta il commento (terza riga)
        getline(file, line);  //salta il commento (quarta riga)

        //inserisce i bus
        _buses.resize(_numBuses);
        for(int b = 0; b < _numBuses; b++)
        {
            getline(file, line);
            istringstream converterBus;
            converterBus.str(line);
            converterBus >> _buses[b].Id >> _buses[b].FuelCost;
        }

        //chiude il file
        file.close();

    }  catch (exception) {
        _numBuses = 0;
        _buses.clear();

        throw runtime_error("Something goes wrong");
    }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if(idBus > _numBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}

void MapData::Reset()
{
  _numberRoutes = 0;
  _numberStreets = 0;
  _numberBusStops = 0;
  _busStops.clear();
  _streets.clear();
  _routes.clear();
  _streetsFrom.clear();
  _streetsTo.clear();
  _routeStreets.clear();
}

void MapData::Load()
{
    //resetta gli attributi
    Reset();

    //apre file
    ifstream file;
    file.open(_mapFilePath.c_str());

    //controlla che file sia stato aperto correttamente
    if (file.fail())
      throw runtime_error("Something goes wrong");

    //load della map
    try {
        string line;
        getline(file, line);  //salta il commento (prima riga)
        getline(file, line);
        istringstream convertN;
        convertN.str(line);
        convertN >> _numberBusStops;

        getline(file, line);  //salta il commento (terza riga)
        //riempie il _busStops
        _busStops.resize(_numberBusStops);
        for(int s = 0; s < _numberBusStops; s++)
        {
            getline(file, line);
            istringstream convertBusStop;
            convertBusStop.str(line);
            convertBusStop >> _busStops[s].Id >> _busStops[s].Name >> _busStops[s].Latitude >> _busStops[s].Longitude;
        }

        getline(file, line);  //salta il commento
        getline(file, line);
        istringstream convertNStreet;
        convertNStreet.str(line);
        convertNStreet >> _numberStreets;

        getline(file, line);  //salta il commento
        //riempie _streets, _streetsFrom, _streetsTo
        _streets.resize(_numberStreets);
        _streetsFrom.resize(_numberStreets);
        _streetsTo.resize(_numberStreets);
        for(int s = 0; s < _numberStreets; s++)
        {
            getline(file, line);
            istringstream convertStreet;
            convertStreet.str(line);
            convertStreet >> _streets[s].Id >> _streetsFrom[s] >>_streetsTo[s] >> _streets[s].TravelTime;
        }

        getline(file, line);  //salta il commento
        getline(file, line);
        istringstream convertNRoute;
        convertNRoute.str(line);
        convertNRoute >> _numberRoutes;

        getline(file, line);  //salta il commento
        //riempie _routes, _routeStreets
        _routes.resize(_numberRoutes);
        _routeStreets.resize(_numberRoutes);
        for(int r = 0; r < _numberRoutes; r++)
        {
            getline(file, line);
            istringstream convertRoutes;
            convertRoutes.str(line);
            convertRoutes >> _routes[r].Id >> _routes[r].NumberStreets;
            _routeStreets[r].resize(_routes[r].NumberStreets);
            for(int i = 0; i < _routes[r].NumberStreets; i++)
                convertRoutes >> _routeStreets[r][i];
        }

        //chiude il file
        file.close();

    }  catch (exception) {
        Reset();

        throw runtime_error("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if(idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    if(streetPosition >= _routes[idRoute-1].NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if(idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute-1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if(idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet-1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if(idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idBusFrom = _streetsFrom[idStreet - 1];
    return _busStops[idBusFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if(idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idBusTo = _streetsTo[idStreet - 1];
    return _busStops[idBusTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if(idBusStop > _numberBusStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop-1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++)
        travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    int costRoute = 0;
    const Bus& busCost = _busStation.GetBus(idBus);
    int travelTimeRoute = ComputeRouteTravelTime(idRoute);
    costRoute = travelTimeRoute * busCost.FuelCost * BusAverageSpeed/3600;

    return costRoute;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int s = 0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; s < route.NumberStreets - 1; s++)
    {
        int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
        string from = _mapData.GetStreetFrom(idStreet).Name;
        routeView << from<< " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
