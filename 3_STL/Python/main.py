class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str, ingr):
        self.Name = name
        self.ingredients = []   #lista in cui ogni elemento è una istanza della classe Ingredient
        self.ingredients.clear()
        for index in ingr:
            self.ingredients.append(index)

    def addIngredient(self, ingredient: Ingredient):
        self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        pricePizza=0
        # ciclo sulla lista di ingredienti della pizza
        for ingredient in self.ingredients:
            pricePizza = pricePizza + ingredient.Price
        return pricePizza


class Order:
    def __init__(self, pizz):
        self.pizzas = []  #vettore in cui ogni elemento è una istanza della classe Pizza
        self.pizzas.clear()
        for index in pizz:
            self.pizzas.append(index)

    def getPizza(self, position: int) -> Pizza:
        # controlla che la posizione sia corretta
        if position > 0 and position <= self.numPizzas():
            return self.pizzas[position - 1]   # -1 perchè vettore parte da 0 ma la posizione è passata da 1
        else:
            raise Exception("Position passed is wrong")

    def initializeOrder(self, numPizzas: int):
        self.pizzas = [None] * numPizzas  #inizializza a numPizzas la lunghezza del vettore

    def addPizza(self, pizza: Pizza):
        self.pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizzas)

    def computeTotal(self) -> int:
        priceOrder = 0
        # ciclo su tutte le pizze dell'ordine
        for pizza in self.pizzas:
            priceOrder = priceOrder + pizza.computePrice()
        return priceOrder


class Pizzeria:
    def __init__(self):
        self.ingredientList = []   #lista in cui ogni elemento è una istanza della classe Ingredient
        self.pizzasList = []    #lista in cui ogni elemento è una istanza della classe Pizza
        self.orders = {}    #dizionario indicizzato da numOrder in cui ogni elemento è una istanza della classe Order

    def addIngredient(self, name: str, description: str, price: int):
        newIngredient = Ingredient(name, price, description)  #è il nuovo ingrediente da aggiungere

        #ricerca se il nuovo ingrediente è già presente
        for ingredient in self.ingredientList:
            if ingredient.Name == name:
                raise Exception("Ingredient already inserted")
        #aggiunge il nuovo ingrediente
        self.ingredientList.append(newIngredient)
        #ordina la lista di ingredienti in ordine alfabetico in base al nome
        self.ingredientList = sorted(self.ingredientList, key=lambda ingredient: ingredient.Name)


    def findIngredient(self, name: str) -> Ingredient:
        # ciclo su tutti gli ingredienti della pizzeria
        for ingredient in self.ingredientList:
            if ingredient.Name == name:
                return ingredient
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        #ricerca se la nuova pizza è già presente
        for pizza in self.pizzasList:
            if pizza.Name == name:
                raise Exception("Pizza already inserted")

        #salva la nuova pizza
        newIngPizza = []    #lista su cui salverà tutti gli ingredienti
        #ciclo su tutti gli ingredienti della pizza da aggiungere
        for ingredient in ingredients:
            foundIng = self.findIngredient(ingredient)  #cerca l'ingrediente
            newIngPizza.append(foundIng)      #salva l'ingrediente in newIngPizza

        newPizza = Pizza(name,newIngPizza)  #crea nuova pizza da aggiungere

        #aggiunge la pizza alla lista
        self.pizzasList.append(newPizza)


    def findPizza(self, name: str) -> Pizza:
        #ciclo su tutte le pizze salvate
        for pizza in self.pizzasList:
            if pizza.Name == name:
                return pizza
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        #controlla se l'ordine è vuoto
        if len(pizzas) == 0:
            raise Exception("Empty order")
        else:  #se l'ordine non è nuovo
            #salva il nuovo ordine
            newPizOrder = []     #lista su cui salverà tutte le pizze
            for pizza in pizzas:
                foundP = self.findPizza(pizza)  #cerca la pizza
                newPizOrder.append(foundP)    #salva la pizza in newPizOrder

            newOrder = Order(newPizOrder)  #crea un nuovo ordine da aggiungere

            numOrd = len(self.orders) + 1000   #restituisce il numero del nuovo ordine
            #aggiunge il nuovo ordine nel dizionario
            self.orders[numOrd] = newOrder
            return numOrd


    def findOrder(self, numOrder: int) -> Order:
        if numOrder in self.orders:
            return self.orders[numOrder]
        else:
            raise Exception("Order not found")



    def getReceipt(self, numOrder: int) -> str:
        foundOrder = self.findOrder(numOrder)   #ricerca l'ordine
        totale = foundOrder.computeTotal()   #costo totale dell'ordine
        nPizze = foundOrder.numPizzas()     #numero di pizze dell'ordine
        receipt = ""

        #ciclo sul numero di pizze dell'ordine
        for i in range(0, nPizze):
            pizzaOrd = foundOrder.getPizza(i + 1)  #restituisce le pizze dell'ordine (+1 perchè getPizza va da 1 in poi ma vettore parte da 0)
            receipt = receipt + "- " + pizzaOrd.Name + ", " + str(pizzaOrd.computePrice()) + " euro" + "\n"
        receipt = receipt + "  TOTAL: " + str(totale) + " euro" + "\n"
        return receipt


    def listIngredients(self) -> str:
        listIng=""
        #ciclo su tutti gli ingredienti della pizzeria
        for element in self.ingredientList:
            listIng = listIng + element.Name + " - '" + element.Description + "': " + str(element.Price) + " euro" + "\n"
        return listIng

    def menu(self) -> str:
        listPizza=""
        #ciclo su tutte le pizze che ho salvato nel menù
        for element in self.pizzasList:
            listPizza = listPizza + element.Name + " (" + str(element.numIngredients()) + " ingredients): " + str(element.computePrice()) + " euro" + "\n"
        return listPizza
