#include "Pizzeria.h"

namespace PizzeriaLibrary {

int Pizza::ComputePrice() const {
    int numIngredients = NumIngredients();  //numero di ingredienti della pizza
    int price = 0;
    //ciclo su tutti gli ingredienti della pizza
    for (int i = 0; i < numIngredients; i++)
    {
        price = price + ingredientsPizza[i].Price;
    }
    return price;
}


const Pizza &Order::GetPizza(const int &position) const {
    int numPizza = NumPizzas();  //numero di pizze nell'ordine
    if (position > 0 && position <= numPizza)
        return pizzas[position - 1];  // -1 perchè vector va da 0 in poi ma posizione è passata da 1
    else
        throw runtime_error("Position passed is wrong");  //passata una posizione oltre il numero di pizze
}


int Order::ComputeTotal() const {
    int numPizzas = NumPizzas();  //numero di pizze nell'ordine
    int priceOrder = 0;
    //ciclo su tutte le pizze dell'ordine
    for (int i = 0; i < numPizzas; i++)
    {
        priceOrder = priceOrder + pizzas[i].ComputePrice();  //per ogni pizza chiamo il metodo ComputePrice per ottenere il prezzo di essa
    }
    return priceOrder;
}


void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {
    Ingredient newIngredient; // newIngredient è il nuovo ingrediente da aggiungere
    newIngredient.Name = name;
    newIngredient.Description = description;
    newIngredient.Price = price;
    list<Ingredient>::const_iterator it;  //iteratore perchè gli ingredienti sono salvati in una lista

    //controlla se il nuovo ingrediente è già presente
    for(it = listIngredients.cbegin(); it != listIngredients.cend(); it++)
    {
        const Ingredient& element = *it;  //referenza all'elemento a cui punta l'iteratore
        if (element.Name == name)
            throw runtime_error("Ingredient already inserted");
    }

    //ricerca la posizione dove inserire il nuovo ingrediente (poichè voglio salvare gli ingredienti in ordine alfabetico)
    for(it = listIngredients.cbegin(); it != listIngredients.cend(); it++)
    {
        const Ingredient& element = *it;
        if (element.Name > name)  //per salvare gli ingredienti in ordine alfabetico
            break;

    }

    //aggiunta del nuovo ingrediente nella posizione individuata della lista
    listIngredients.insert(it, newIngredient);
}


const Ingredient &Pizzeria::FindIngredient(const string &name) const {
    //ciclo su tutti gli ingredienti della pizzeria
    for(list<Ingredient>::const_iterator it = listIngredients.cbegin(); it != listIngredients.cend(); it++)
    {
        const Ingredient& element = *it;   //referenza all'elemento a cui punta l'iteratore
        if (element.Name == name)  //se ho trovato ingrediente
            return element;
    }
    throw runtime_error("Ingredient not found");
}


void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {
    Pizza newPizza; //newPizza è la nuova pizza da aggiungere
    newPizza.Name = name;
    int numIng = ingredients.size();  //numero di ingredienti della pizza da aggiungere
    newPizza.ingredientsPizza.resize(numIng);  //inizializzo a numIng la lunghezza del vettore di ingredienti di newPizza

    //ciclo su tutti gli ingredienti della pizza da aggiungere
    for (int i = 0; i < numIng; i++)
    {
        // ricerca l'ingrediente e lo salva in newPizza
        Ingredient newIng = FindIngredient(ingredients[i]); //cerca i-esimo elemento del vettore ingredients tra gli ingredienti della pizzeria e restituisce l'istanza dell'ingrediente
        //salvo l'ingrediente in newPizza
        newPizza.ingredientsPizza[i].Name = newIng.Name;
        newPizza.ingredientsPizza[i].Description = newIng.Description;
        newPizza.ingredientsPizza[i].Price = newIng.Price;
    }

    //ricerca se la pizza è già presente
    for(list<Pizza>::const_iterator it = menuPizzeria.cbegin(); it != menuPizzeria.cend(); it++)
    {
        const Pizza& element = *it;
        if (element.Name == name)
            throw runtime_error("Pizza already inserted");
    }

    //aggiunge la nuova pizza in testa alla lista (se non è già presente)
    menuPizzeria.push_front(newPizza);
}


const Pizza &Pizzeria::FindPizza(const string &name) const {
    //ciclo su tutte le pizze che ho salvato nel menù
    for(list<Pizza>::const_iterator it = menuPizzeria.cbegin(); it != menuPizzeria.cend(); it++)
    {
        const Pizza& element = *it;
        if (element.Name == name)   //se ho trovato pizza
            return element;
    }
    throw runtime_error("Pizza not found");
}


int Pizzeria::CreateOrder(const vector<string> &pizzas) {
    //controllo se ordine è vuoto
    if (pizzas.empty())
        throw runtime_error("Empty order");
    else //se ordine non è vuoto
    {
        Order newOrder;  //newOrder è il nuovo ordine da aggiungere
        int numPizza = pizzas.size();  //numero di pizze del nuovo ordine (pizze passate in input)
        newOrder.pizzas.resize(numPizza);  //inizializzo a numPizza la lunghezza del vettore di pizze di newOrder
        //ciclo su tutte le pizze passate in input
        for (int i = 0; i < numPizza; i++)
        {
            //ricerca la pizza e la salva in newOrder
            Pizza newPizza = FindPizza(pizzas[i]);
            newOrder.pizzas[i].Name = newPizza.Name;
            newOrder.pizzas[i].ingredientsPizza = newPizza.ingredientsPizza;
        }

        int numOrd = orders.size(); //restituisce la dimensione del vettore orders fino ad adesso per poter assegnare al nuovo ordine un numero
        orders.push_back(newOrder); //aggiunge il nuovo ordine in coda al vettore di ordini
        numOrders.push_back(1000 + numOrd); //aggiunge il nuovo numero di ordine a numOrders
        return numOrders[numOrd];
    }
}


const Order &Pizzeria::FindOrder(const int &numOrder) const {
    //controlla che indice passato sia giusto
    if((numOrder-1000)<0 || (numOrder-1000)>=orders.size())
        throw runtime_error("Order not found");
    else
        return orders[numOrder-1000];
}


string Pizzeria::GetReceipt(const int &numOrder) const {
    string receipt = "";
    Order foundOrder = FindOrder(numOrder); //ricerca l'ordine
    int numPiz = foundOrder.NumPizzas(); //numero di pizze dell'ordine
    int  totale = foundOrder.ComputeTotal(); //costo totale dell'ordine
    //ciclo su tutte le pizze dell'ordine
    for (int i = 0; i < numPiz; i++)
   {
        Pizza pizzaOrder = foundOrder.GetPizza(i+1); //restituisce le pizze dell'ordine (+1 perchè GetPizza va da 1 in poi ma vector parte da 0)
        receipt = receipt + "- " + pizzaOrder.Name + ", " + to_string( pizzaOrder.ComputePrice()) + " euro" + "\n";

   }
   receipt = receipt + "  TOTAL: " + to_string(totale) + " euro" + "\n";

   return receipt;
}


string Pizzeria::ListIngredients() const {
    string listIngr = "";
    //ciclo su tutti gli ingredienti della pizzeria
    for(list<Ingredient>::const_iterator it = listIngredients.cbegin(); it != listIngredients.cend(); it++)
    {
        const Ingredient& element = *it;
        listIngr = listIngr + element.Name +  " - '" + element.Description + "': " + to_string(element.Price) + " euro" + "\n";
    }
    return listIngr;
}


string Pizzeria::Menu() const {
    string listPizzas = "";
    //ciclo su tutte le pizze che ho salvato nel menù
    for(list<Pizza>::const_iterator it = menuPizzeria.cbegin(); it != menuPizzeria.cend(); it++)
    {
        const Pizza& element = *it;
        listPizzas = listPizzas + element.Name +  " (" + to_string(element.NumIngredients()) + " ingredients): " + to_string(element.ComputePrice()) + " euro" + "\n";
    }
    return listPizzas;
}


}
