#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
  public:
      string Name;
      vector<Ingredient> ingredientsPizza;  //vettore in cui ogni elemento è una istanza della classe Ingredient

      void AddIngredient(const Ingredient& ingredient) { ingredientsPizza.push_back(ingredient); }
      int NumIngredients() const { return ingredientsPizza.size(); }
      int ComputePrice() const;
  };

  class Order {
  public:
      vector<Pizza> pizzas;  //vettore in cui ogni elemento è una istanza della classe Pizza

      void InitializeOrder(int numPizzas) { pizzas.reserve(numPizzas);} //inizializzo a numPizzas la lunghezza del vettore
      void AddPizza(const Pizza& pizza) { pizzas.push_back(pizza); }  //aggiunto pizza alla fine del vettore pizzas
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const { return pizzas.size(); }  //numero di pizze nell'ordine
      int ComputeTotal() const;
  };

  class Pizzeria {
    private:
      list<Ingredient> listIngredients;  //lista in cui ogni elemento è una istanza della classe Ingredient
      list<Pizza> menuPizzeria;   //lista in cui ogni elemento è una istanza della classe Pizza
      vector<Order> orders;   //vettore in cui ogni elemento è una istanza della classe Order
      vector<int> numOrders;  //vettore in cui ogni elemento è un intero per identificare gli ordini

    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };

};

#endif // PIZZERIA_H
