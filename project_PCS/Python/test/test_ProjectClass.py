from unittest import TestCase
import src.ProjectClass as project

class TestPolygon(TestCase):
    def test_CreatePolygonEdges(self):
        vertices = []
        vertices.clear()
        vertices.append(project.Point(0.0, 0.0))
        vertices.append(project.Point(2.0, 0.0))
        vertices.append(project.Point(2.0, 3.0))

        polygonNew = project.Polygon(vertices)

        self.assertEqual(polygonNew.createPolygonEdges(vertices)[0].From.X, 0.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[0].From.Y, 0.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[0].To.X, 2.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[0].To.Y, 0.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[1].From.X, 2.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[1].From.Y, 0.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[1].To.X, 2.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[1].To.Y, 3.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[2].From.X, 2.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[2].From.Y, 3.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[2].To.X, 0.0)
        self.assertEqual(polygonNew.createPolygonEdges(vertices)[2].To.Y, 0.0)

class TestPolygonFactory(TestCase):
    def test_CutPolygon1(self):
        points = []
        points.clear()
        points.append(project.Point(5.0, 3.1))
        points.append(project.Point(1.0, 1.0))
        points.append(project.Point(1.0, 3.1))
        points.append(project.Point(5.0, 1.0))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(1)
        polygonVertices.append(3)
        polygonVertices.append(0)
        polygonVertices.append(2)

        origin = project.Point(2.0, 1.2)
        to = project.Point(4.0, 3.0)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X,points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y,points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X,points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y,points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X,points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y,points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X,points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y,points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X,1.7777777777777781)
        self.assertEqual(newPolygonFactory.newPoint[4].Y, 1)
        self.assertEqual(newPolygonFactory.newPoint[5].X,4.1111111111111107)
        self.assertEqual(newPolygonFactory.newPoint[5].Y, 3.1)

    def test_CreatePolygon1(self):
        points = []
        points.clear()
        points.append(project.Point(5.0, 3.1))
        points.append(project.Point(1.0, 1.0))
        points.append(project.Point(1.0, 3.1))
        points.append(project.Point(5.0, 1.0))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(1)
        polygonVertices.append(3)
        polygonVertices.append(0)
        polygonVertices.append(2)

        origin = project.Point(2.0, 1.2)
        to = project.Point(4.0, 3.0)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 2)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][3], 4)

    def test_CutPolygon2(self):
        points = []
        points.clear()
        points.append(project.Point(1.6, 4.2))
        points.append(project.Point(3.4, 4.2))
        points.append(project.Point(1.0, 2.1))
        points.append(project.Point(2.5, 1.0))
        points.append(project.Point(4.0, 2.1))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(3)
        polygonVertices.append(4)
        polygonVertices.append(1)
        polygonVertices.append(0)
        polygonVertices.append(2)

        origin = project.Point(1.4, 2.75)
        to = project.Point(3.6, 2.2)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X,points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y,points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X,points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y,points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X,points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y,points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X,points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y,points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X,points[4].X)
        self.assertEqual(newPolygonFactory.newPoint[4].Y,points[4].Y)
        self.assertEqual(newPolygonFactory.newPoint[5].X,1.2)
        self.assertEqual(newPolygonFactory.newPoint[5].Y,2.8000000000000003)
        self.assertEqual(newPolygonFactory.newPoint[6].X,4.0)
        self.assertEqual(newPolygonFactory.newPoint[6].Y,2.1000000000000005)


    def test_CreatePolygon2(self):
        points = []
        points.clear()
        points.append(project.Point(1.6, 4.2))
        points.append(project.Point(3.4, 4.2))
        points.append(project.Point(1.0, 2.1))
        points.append(project.Point(2.5, 1.0))
        points.append(project.Point(4.0, 2.1))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(3)
        polygonVertices.append(4)
        polygonVertices.append(1)
        polygonVertices.append(0)
        polygonVertices.append(2)

        origin = project.Point(1.4, 2.75)
        to = project.Point(3.6, 2.2)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 6)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 2)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][3], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][4], 6)


    def test_CutPolygon3(self):
        points = []
        points.clear()
        points.append(project.Point(1.0,4.0))
        points.append(project.Point(5.5,4.8))
        points.append(project.Point(3.2,4.2))
        points.append(project.Point(4.0,6.2))
        points.append(project.Point(1.5,1.0))
        points.append(project.Point(5.6, 1.5))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(4)
        polygonVertices.append(5)
        polygonVertices.append(1)
        polygonVertices.append(3)
        polygonVertices.append(2)
        polygonVertices.append(0)

        origin = project.Point(2.0, 3.7)
        to = project.Point(4.1, 5.9)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X, points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y, points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X, points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y, points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X, points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y, points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X, points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y, points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X, points[4].X)
        self.assertEqual(newPolygonFactory.newPoint[4].Y, points[4].Y)
        self.assertEqual(newPolygonFactory.newPoint[5].X, points[5].X)
        self.assertEqual(newPolygonFactory.newPoint[5].Y, points[5].Y)
        self.assertEqual(newPolygonFactory.newPoint[6].X, 1.1912162162162161)
        self.assertEqual(newPolygonFactory.newPoint[6].Y, 2.8527027027027021)
        self.assertEqual(newPolygonFactory.newPoint[7].X, 2.4085972850678732)
        self.assertEqual(newPolygonFactory.newPoint[7].Y, 4.1280542986425335)
        self.assertEqual(newPolygonFactory.newPoint[8].X, 3.7213114754098369)
        self.assertEqual(newPolygonFactory.newPoint[8].Y, 5.503278688524591)
        self.assertEqual(newPolygonFactory.newPoint[9].X, 4.2043269230769225)
        self.assertEqual(newPolygonFactory.newPoint[9].Y, 6.0092948717948724)

    def test_CreatePolygon3(self):
        points = []
        points.clear()
        points.append(project.Point(1.0, 4.0))
        points.append(project.Point(5.5, 4.8))
        points.append(project.Point(3.2, 4.2))
        points.append(project.Point(4.0, 6.2))
        points.append(project.Point(1.5, 1.0))
        points.append(project.Point(5.6, 1.5))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(4)
        polygonVertices.append(5)
        polygonVertices.append(1)
        polygonVertices.append(3)
        polygonVertices.append(2)
        polygonVertices.append(0)

        origin = project.Point(2.0, 3.7)
        to = project.Point(4.1, 5.9)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 9)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][4], 8)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][5], 2)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][6], 7)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][7], 6)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 8)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 9)

        self.assertEqual(newPolygonFactory.cuttedPolygons[2][0], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][1], 6)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][2], 7)


    def test_CutPolygon4(self):
        points = []
        points.clear()
        points.append(project.Point(5.38, 5.77))
        points.append(project.Point(9.92, 3.8))
        points.append(project.Point(2.0, 3.0))
        points.append(project.Point(5.19, 1.47))
        points.append(project.Point(9.8, 1.48))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(2)
        polygonVertices.append(3)
        polygonVertices.append(4)
        polygonVertices.append(1)
        polygonVertices.append(0)

        origin = project.Point(3.66, 6.01)
        to = project.Point(6.45, 2.72)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X, points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y, points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X, points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y, points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X, points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y, points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X, points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y, points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X, points[4].X)
        self.assertEqual(newPolygonFactory.newPoint[4].Y, points[4].Y)
        self.assertEqual(newPolygonFactory.newPoint[5].X, 4.48531363238454)
        self.assertEqual(newPolygonFactory.newPoint[5].Y, 5.0367806987293422)
        self.assertEqual(newPolygonFactory.newPoint[6].X, 7.5057704609471658)
        self.assertEqual(newPolygonFactory.newPoint[6].Y, 1.4750233632558509)


    def test_CreatePolygon4(self):
        points = []
        points.clear()
        points.append(project.Point(5.38, 5.77))
        points.append(project.Point(9.92, 3.8))
        points.append(project.Point(2.0, 3.0))
        points.append(project.Point(5.19, 1.47))
        points.append(project.Point(9.8, 1.48))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(2)
        polygonVertices.append(3)
        polygonVertices.append(4)
        polygonVertices.append(1)
        polygonVertices.append(0)

        origin = project.Point(3.66, 6.01)
        to = project.Point(6.45, 2.72)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 2)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 6)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 5)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][3], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][4], 6)


    def test_CutPolygon5(self):
        points = []
        points.clear()
        points.append(project.Point(4.0, 6.32))
        points.append(project.Point(5.98, 4.58))
        points.append(project.Point(1.86, 4.56))
        points.append(project.Point(2.61, 5.4))
        points.append(project.Point(3.23, 1.78))
        points.append(project.Point(7.34, 2.02))
        points.append(project.Point(8.45, 4.64))
        points.append(project.Point(6.93, 6.38))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(4)
        polygonVertices.append(5)
        polygonVertices.append(6)
        polygonVertices.append(7)
        polygonVertices.append(1)
        polygonVertices.append(0)
        polygonVertices.append(3)
        polygonVertices.append(2)

        origin = project.Point(1.4, 5.18)
        to = project.Point(8.78, 5.46)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X, points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y, points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X, points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y, points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X, points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y, points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X, points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y, points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X, points[4].X)
        self.assertEqual(newPolygonFactory.newPoint[4].Y, points[4].Y)
        self.assertEqual(newPolygonFactory.newPoint[5].X, points[5].X)
        self.assertEqual(newPolygonFactory.newPoint[5].Y, points[5].Y)
        self.assertEqual(newPolygonFactory.newPoint[6].X, points[6].X)
        self.assertEqual(newPolygonFactory.newPoint[6].Y, points[6].Y)
        self.assertEqual(newPolygonFactory.newPoint[7].X, points[7].X)
        self.assertEqual(newPolygonFactory.newPoint[7].Y, points[7].Y)
        self.assertEqual(newPolygonFactory.newPoint[8].X, 2.4491103987176914)
        self.assertEqual(newPolygonFactory.newPoint[8].Y, 5.2198036465638147)
        self.assertEqual(newPolygonFactory.newPoint[9].X, 5.1359473259876376)
        self.assertEqual(newPolygonFactory.newPoint[9].Y, 5.3217432589805611)
        self.assertEqual(newPolygonFactory.newPoint[10].X, 6.3967214625902589)
        self.assertEqual(newPolygonFactory.newPoint[10].Y, 5.3695775080657553)
        self.assertEqual(newPolygonFactory.newPoint[11].X, 7.7672442488015179)
        self.assertEqual(newPolygonFactory.newPoint[11].Y, 5.4215756625561546)

    def test_CreatePolygon5(self):
        points = []
        points.clear()
        points.append(project.Point(4.0, 6.32))
        points.append(project.Point(5.98, 4.58))
        points.append(project.Point(1.86, 4.56))
        points.append(project.Point(2.61, 5.4))
        points.append(project.Point(3.23, 1.78))
        points.append(project.Point(7.34, 2.02))
        points.append(project.Point(8.45, 4.64))
        points.append(project.Point(6.93, 6.38))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(4)
        polygonVertices.append(5)
        polygonVertices.append(6)
        polygonVertices.append(7)
        polygonVertices.append(1)
        polygonVertices.append(0)
        polygonVertices.append(3)
        polygonVertices.append(2)

        origin = project.Point(1.4, 5.18)
        to = project.Point(8.78, 5.46)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 6)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 11)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][4], 10)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][5], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][6], 9)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][7], 8)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][8], 2)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 7)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 10)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 11)

        self.assertEqual(newPolygonFactory.cuttedPolygons[2][0], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][1], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][2], 8)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][3], 9)

    def test_CutPolygon6(self):
        points = []
        points.clear()
        points.append(project.Point(1.85, 1.08))
        points.append(project.Point(4.64, 1.07))
        points.append(project.Point(5.41, 3.24))
        points.append(project.Point(3.78, 2.60))
        points.append(project.Point(2.41, 4.16))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(0)
        polygonVertices.append(1)
        polygonVertices.append(2)
        polygonVertices.append(3)
        polygonVertices.append(4)

        origin = project.Point(1.71, 3.74)
        to = project.Point(5.41, 3.24)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)

        self.assertEqual(newPolygonFactory.newPoint[0].X, points[0].X)
        self.assertEqual(newPolygonFactory.newPoint[0].Y, points[0].Y)
        self.assertEqual(newPolygonFactory.newPoint[1].X, points[1].X)
        self.assertEqual(newPolygonFactory.newPoint[1].Y, points[1].Y)
        self.assertEqual(newPolygonFactory.newPoint[2].X, points[2].X)
        self.assertEqual(newPolygonFactory.newPoint[2].Y, points[2].Y)
        self.assertEqual(newPolygonFactory.newPoint[3].X, points[3].X)
        self.assertEqual(newPolygonFactory.newPoint[3].Y, points[3].Y)
        self.assertEqual(newPolygonFactory.newPoint[4].X, points[4].X)
        self.assertEqual(newPolygonFactory.newPoint[4].Y, points[4].Y)
        self.assertEqual(newPolygonFactory.newPoint[5].X, 2.3186810551558756)
        self.assertEqual(newPolygonFactory.newPoint[5].Y, 3.6577458033573143)
        self.assertEqual(newPolygonFactory.newPoint[6].X, 2.9227737369766071)
        self.assertEqual(newPolygonFactory.newPoint[6].Y, 3.5761116571653231)
        self.assertEqual(newPolygonFactory.newPoint[7].X, 5.41)
        self.assertEqual(newPolygonFactory.newPoint[7].Y, 3.24)
        self.assertEqual(newPolygonFactory.newPoint[8].X, 5.41)
        self.assertEqual(newPolygonFactory.newPoint[8].Y, 3.24)

    def test_CreatePolygon6(self):
        points = []
        points.clear()
        points.append(project.Point(1.85, 1.08))
        points.append(project.Point(4.64, 1.07))
        points.append(project.Point(5.41, 3.24))
        points.append(project.Point(3.78, 2.60))
        points.append(project.Point(2.41, 4.16))

        polygonVertices = []
        polygonVertices.clear()
        polygonVertices.append(0)
        polygonVertices.append(1)
        polygonVertices.append(2)
        polygonVertices.append(3)
        polygonVertices.append(4)

        origin = project.Point(1.71, 3.74)
        to = project.Point(5.41, 3.24)
        segment = project.Segment(origin, to)

        newPolygonFactory = project.PolygonFactory()
        newPolygonFactory.CutPolygon(points, polygonVertices, segment)
        newPolygonFactory.CreatePolygons(polygonVertices)

        self.assertEqual(newPolygonFactory.cuttedPolygons[0][0], 0)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][1], 1)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][2], 8)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][3], 7)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][4], 3)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][5], 6)
        self.assertEqual(newPolygonFactory.cuttedPolygons[0][6], 5)

        self.assertEqual(newPolygonFactory.cuttedPolygons[1][0], 2)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][1], 7)
        self.assertEqual(newPolygonFactory.cuttedPolygons[1][2], 8)

        self.assertEqual(newPolygonFactory.cuttedPolygons[2][0], 4)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][1], 5)
        self.assertEqual(newPolygonFactory.cuttedPolygons[2][2], 6)
