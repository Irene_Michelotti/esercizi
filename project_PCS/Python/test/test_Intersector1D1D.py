from unittest import TestCase

import src.Intersector1D1D as intersec
from src.Intersector1D1D import Vector2d


class TestIntersector(TestCase):
    def test_ParallelIntersection(self):
        a =  Vector2d(0, 0)
        b =  Vector2d(4, 0)
        c =  Vector2d(1, 0)
        d =  Vector2d(2, 0)

        intersectorNew = intersec.Intersector1D1D()
        intersectorNew.SetFirstSegment(a, b)
        intersectorNew.SetSecondSegment(c, d)

        self.assertTrue(intersectorNew.computeIntersection())
        self.assertEqual(intersectorNew.TypeIntersection(), intersec.Type.IntersectionParallelOnSegment)
        self.assertEqual(0.25,intersectorNew.FirstParametricCoordinate())
        self.assertEqual(0.5, intersectorNew.SecondParametricCoordinate())

    def test_SegmentIntersection(self):
        a = Vector2d(1, 0)
        b = Vector2d(5, 0)
        c = Vector2d(3, -6)
        d = Vector2d(3, 6)

        intersector = intersec.Intersector1D1D()
        intersector.SetFirstSegment(a, b)
        intersector.SetSecondSegment(c, d)

        self.assertTrue(intersector.computeIntersection())
        self.assertEqual(intersector.TypeIntersection(), intersec.Type.IntersectionOnSegment)
        self.assertEqual(0.5, intersector.FirstParametricCoordinate())
        self.assertEqual(0.5, intersector.SecondParametricCoordinate())
        self.assertLessEqual(intersector.FirstParametricCoordinate(), 1.0)
        self.assertGreaterEqual(intersector.FirstParametricCoordinate(), 0.0)

    def test_OnLineIntersection(self):
        a = Vector2d(3, 6)
        b = Vector2d(3, 2)
        c = Vector2d(5, 0)
        d = Vector2d(1, 0)

        intersector2 = intersec.Intersector1D1D()
        intersector2.SetFirstSegment(a, b)
        intersector2.SetSecondSegment(c, d)

        self.assertTrue(intersector2.computeIntersection())
        self.assertEqual(intersector2.TypeIntersection(), intersec.Type.IntersectionOnLine)
        self.assertEqual(1.5, intersector2.FirstParametricCoordinate())
        self.assertEqual(0.5, intersector2.SecondParametricCoordinate())
        self.assertGreaterEqual(intersector2.FirstParametricCoordinate(), 1.0)

