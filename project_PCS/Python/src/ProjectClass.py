import src.Intersector1D1D as intersec

class Point:
    def __init__(self,x,y):
        self.X = x
        self.Y = y


class Segment:
    def __init__(self, origin: Point, to: Point):
        self.From = Point(origin.X, origin.Y)
        self.To = Point(to.X, to.Y)


class Polygon:
    def __init__(self,vertices):
        self.Vertices = []
        self.Vertices.clear()
        for vertex in vertices:
            self.Vertices.append(vertex)

    def createPolygonEdges(self, vertices: []):
        NumVertices : int = len(vertices)
        Edges = []
        Edges.clear()

        for i in range(0, NumVertices - 1):
            Edges.append(Segment(vertices[i], vertices[i+1]))

        Edges.append(Segment(vertices[NumVertices - 1], vertices[0]))

        return Edges

class IPolygonFactory:
    def CutPolygon(self, points:[], polygonVertices:[], segment: Segment):
        pass
    def CreatePolygons(self,polygonVertices):
        pass

class PolygonFactory(IPolygonFactory):
    def __init__(self):
        self.newPoint = []  #vettore di punti
        self.segmentIntersection = []   #vettore di interi
        self.parametricCoordinateIntersections = []    #vettore di double
        self.orderedIntersectionPoint = []   #vettore di Point
        self.interToSegment = []     #vettore di int

        self.cuttedPolygons = []   #vettore di vettori di interi
        self.visitedEdge = []    #vettore di bool

    def CutPolygon(self, points:[], polygonVertices:[], segment: Segment):
        orderedPoints = []  #vettore di punti
        orderedPoints.clear()
        NumVertices = len(polygonVertices)
        for i in range(0, NumVertices):
            position = polygonVertices[i]
            orderedPoints.append(Point(points[position].X,points[position].Y ))

        poligono = Polygon(orderedPoints)
        orderedSegments = []
        orderedSegments.clear()

        for edges in poligono.createPolygonEdges(orderedPoints):
            orderedSegments.append(edges)


        self.newPoint.clear()
        for vertex in points:
            self.newPoint.append(vertex)

        intersector = intersec.Intersector1D1D()
        originSegment = intersec.Vector2d(segment.From.X, segment.From.Y)
        endSegment = intersec.Vector2d(segment.To.X, segment.To.Y)
        intersector.SetFirstSegment(originSegment, endSegment)

        self.parametricCoordinateIntersections.clear()
        self.orderedIntersectionPoint.clear()
        self.interToSegment.clear()


        self.segmentIntersection = [0]*NumVertices
        i = 0

        while i < NumVertices:
            originEdge = intersec.Vector2d(orderedSegments[i].From.X, orderedSegments[i].From.Y)
            endEdge = intersec.Vector2d(orderedSegments[i].To.X, orderedSegments[i].To.Y)
            intersector.SetSecondSegment(originEdge, endEdge)

            isIntersection = intersector.computeIntersection()
            position = intersector.PositionIntersectionInSecondEdge()

            if ((isIntersection == True) and (position == intersec.Position.End or
                                             position == intersec.Position.Inner)):

                tolerance = 1.0E-07
                if (position == intersec.Position.End):
                    vecPQ = intersec.Vector2d(segment.From.X- orderedPoints[i+1].X, segment.From.Y- orderedPoints[i+1].Y)
                    vecPRprec = intersec.Vector2d(orderedPoints[i].X-orderedPoints[i+1].X, orderedPoints[i].Y-orderedPoints[i+1].Y)
                    vecPRsucc = intersec.Vector2d(orderedPoints[i+2].X-orderedPoints[i+1].X, orderedPoints[i+2].Y-orderedPoints[i+1].Y)
                    crossPrec = vecPQ.x * vecPRprec.y - vecPQ.y * vecPRprec.x
                    crossSucc = vecPQ.x * vecPRsucc.y - vecPQ.y * vecPRsucc.x

                    if ((crossPrec < -tolerance and crossSucc < -tolerance) or (crossPrec > tolerance and crossSucc > tolerance)):
                        self.parametricCoordinateIntersections.append(intersector.FirstParametricCoordinate())
                        intersectionVector1 = intersector.IntersectionFirstParametricCoordinate(originSegment,endSegment)
                        intersectionPoint1 = Point(intersectionVector1.x, intersectionVector1.y)
                        self.newPoint.append(intersectionPoint1)
                        self.orderedIntersectionPoint.append(intersectionPoint1)
                        self.interToSegment.append(i)

                        self.parametricCoordinateIntersections.append(intersector.FirstParametricCoordinate())
                        intersectionVector2 = intersector.IntersectionFirstParametricCoordinate(originSegment,endSegment)
                        intersectionPoint2 = Point(intersectionVector2.x, intersectionVector2.y)
                        self.newPoint.append(intersectionPoint2)
                        self.orderedIntersectionPoint.append(intersectionPoint2)
                        self.interToSegment.append(i+1)
                        i+=1

                    else:
                        self.parametricCoordinateIntersections.append(intersector.FirstParametricCoordinate())
                        intersectionVector = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment)
                        intersectionPoint = Point(intersectionVector.x, intersectionVector.y)
                        self.newPoint.append(intersectionPoint)
                        self.orderedIntersectionPoint.append(intersectionPoint)
                        self.interToSegment.append(i)

                else:
                    self.parametricCoordinateIntersections.append(intersector.FirstParametricCoordinate())
                    intersectionVector = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment)
                    intersectionPoint = Point(intersectionVector.x, intersectionVector.y)
                    self.newPoint.append(intersectionPoint)
                    self.orderedIntersectionPoint.append(intersectionPoint)
                    self.interToSegment.append(i)


                n = len(self.parametricCoordinateIntersections)

                for j in range (0,n):
                    if self.parametricCoordinateIntersections[n - 1] <= self.parametricCoordinateIntersections[j]:
                        tempCoord = self.parametricCoordinateIntersections[n - 1]
                        tempPoint = self.orderedIntersectionPoint[n - 1]
                        tempIntSeg = self.interToSegment[n - 1]
                        tempNewPoint = self.newPoint[NumVertices + n - 1]

                        for k in range (n-1,j,-1):
                            self.parametricCoordinateIntersections[k] = self.parametricCoordinateIntersections[k - 1]
                            self.orderedIntersectionPoint[k] =self.orderedIntersectionPoint[k - 1]
                            self.interToSegment[k] = self.interToSegment[k - 1]
                            self.newPoint[NumVertices + k] = self.newPoint[NumVertices + k - 1]

                        self.parametricCoordinateIntersections[j] = tempCoord
                        self.orderedIntersectionPoint[j]= tempPoint
                        self.interToSegment[j] = tempIntSeg
                        self.newPoint[NumVertices+j] = tempNewPoint
            else:
                self.segmentIntersection[i]=-1

            i+=1

        for i in range(0,NumVertices):
            if self.segmentIntersection[i] != -1:
                for j in range (0,len(self.interToSegment)):
                    if self.interToSegment[j] == i:
                        self.segmentIntersection[i] = j



    def CreatePolygons(self, polygonVertices):
        self.cuttedPolygons.clear()
        self.visitedEdge.clear()

        for i in range(0, len(polygonVertices)):
            self.visitedEdge.append(False)

        numberVertices = len(polygonVertices)
        for i in range(0, numberVertices):
            if self.visitedEdge[i] == False:
                j = i
                newPolygon = []
                newPolygon.clear()
                self.visitedEdge[i] = True
                newPolygon.append(polygonVertices[i])

                flag = False
                while polygonVertices[(j + 1) % numberVertices] != polygonVertices[i] and flag == False:
                    if self.segmentIntersection[j] != -1:
                        if self.segmentIntersection[j] % 2 == 0:
                            interPointId = numberVertices + self.segmentIntersection[j]
                            newPolygon.append(interPointId)
                            newPolygon.append(interPointId+1)
                            j = 1 + self.interToSegment[self.segmentIntersection[j] + 1]
                            if polygonVertices[j % numberVertices] != polygonVertices[i]:
                                self.visitedEdge[j % numberVertices] = True
                                newPolygon.append(polygonVertices[j % numberVertices])
                            else:
                                flag = True
                        else:
                            interPointId = len(polygonVertices) + self.segmentIntersection[j]
                            newPolygon.append(interPointId)
                            newPolygon.append(interPointId-1)
                            j = 1 + self.interToSegment[self.segmentIntersection[j] - 1]

                            if polygonVertices[j % numberVertices] != polygonVertices[i]:
                                self.visitedEdge[j % numberVertices] = True
                                newPolygon.append(polygonVertices[j % numberVertices])
                            else:
                                flag = True
                    else:
                        self.visitedEdge[(j + 1) % numberVertices] = True
                        newPolygon.append(polygonVertices[(j + 1) % numberVertices])
                        j+=1

                self.cuttedPolygons.append(newPolygon)