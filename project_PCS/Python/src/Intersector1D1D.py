import math
from enum import Enum


class Type(Enum):
    NoIntersection = 0
    IntersectionOnLine = 1
    IntersectionOnSegment = 2
    IntersectionParallelOnLine = 3
    IntersectionParallelOnSegment = 4


class Position(Enum):
    Begin = 0
    Inner = 1
    End = 2
    Outer = 3


class Vector2d:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def squaredNorm(self):
        return pow(self.x, 2) + pow(self.y, 2)

class Matrix2d:
    def __init__(self, col1: Vector2d, col2: Vector2d):
        self.col1 = col1
        self.col2 = col2

    def det(self):
        return (self.col1.x * self.col2.y) - (self.col1.y * self.col2.x)


class Intersector1D1D:
    def __init__(self):
        self.toleranceParallelism = 1.0E-7
        self.toleranceIntersection = 1.0E-7
        self.type = Type.NoIntersection
        self.positionIntersectionFirstEdge = Position.Outer
        self.positionIntersectionSecondEdge = Position.Outer
        self.resultParametricCoordinates = Vector2d(0, 0)
        self.originFirstSegment = Vector2d(0, 0)
        self.rightHandSide = Vector2d(0, 0)
        self.matrixTangentVector = Matrix2d(Vector2d(0, 0), Vector2d(0, 0))

    def SetToleranceParallelism(self, _tolerance):
        self.toleranceParallelism = _tolerance

    def SetToleranceIntersection(self, _tolerance):
        self.toleranceIntersection = _tolerance

    def SetFirstSegment(self, origin: Vector2d, end: Vector2d):
        self.matrixTangentVector.col1.x = end.x - origin.x
        self.matrixTangentVector.col1.y = end.y - origin.y
        self.originFirstSegment.x = origin.x
        self.originFirstSegment.y = origin.y

    def SetSecondSegment(self, origin: Vector2d, end: Vector2d):
        self.matrixTangentVector.col2.x = origin.x - end.x
        self.matrixTangentVector.col2.y = origin.y - end.y
        self.rightHandSide.x = origin.x - self.originFirstSegment.x
        self.rightHandSide.y = origin.y - self.originFirstSegment.y

    def ToleranceIntersection(self):
        return self.toleranceIntersection

    def ToleranceParallelism(self):
        return self.toleranceParallelism

    def ParametricCoordinates(self):
        return self.resultParametricCoordinates

    def FirstParametricCoordinate(self):
        return self.resultParametricCoordinates.x

    def SecondParametricCoordinate(self):
        return self.resultParametricCoordinates.y

    def PositionIntersectionInFirstEdge(self):
        return self.positionIntersectionFirstEdge

    def PositionIntersectionInSecondEdge(self):
        return self.positionIntersectionSecondEdge

    def TypeIntersection(self):
        return self.type

    def IntersectionFirstParametricCoordinate(self, origin, end):
        return Vector2d((1 - self.resultParametricCoordinates.x) * origin.x + self.resultParametricCoordinates.x * end.x, (1 - self.resultParametricCoordinates.x) * origin.y + self.resultParametricCoordinates.x * end.y)

    def IntersectionSecondParametricCoordinate(self, origin, end):
        return Vector2d((1 - self.resultParametricCoordinates.y) * origin.x + self.resultParametricCoordinates.y * end.x, (1 - self.resultParametricCoordinates.y) * origin.y + self.resultParametricCoordinates.x * end.y)

    def computeIntersection(self):
        parallelism = self.matrixTangentVector.det()
        self.type = Type.NoIntersection
        intersection = False
        check = self.toleranceParallelism * self.toleranceParallelism * \
                self.matrixTangentVector.col1.squaredNorm() * \
                self.matrixTangentVector.col2.squaredNorm()

        if parallelism * parallelism >= check:
            solverMatrix = Matrix2d(Vector2d(0, 0), Vector2d(0, 0))
            solverMatrix.col1.x = self.matrixTangentVector.col2.y
            solverMatrix.col2.x = -self.matrixTangentVector.col2.x
            solverMatrix.col1.y = -self.matrixTangentVector.col1.y
            solverMatrix.col2.y = self.matrixTangentVector.col1.x

            self.resultParametricCoordinates.x = solverMatrix.col1.x * self.rightHandSide.x + solverMatrix.col2.x * self.rightHandSide.y
            self.resultParametricCoordinates.y = solverMatrix.col1.y * self.rightHandSide.x + solverMatrix.col2.y * self.rightHandSide.y
            self.resultParametricCoordinates.x = self.resultParametricCoordinates.x / parallelism
            self.resultParametricCoordinates.y = self.resultParametricCoordinates.y / parallelism

            if self.resultParametricCoordinates.y > -self.toleranceIntersection and self.resultParametricCoordinates.y - 1.0 < self.toleranceIntersection:
                self.type = Type.IntersectionOnLine
                intersection = True
                if self.resultParametricCoordinates.x > -self.toleranceIntersection and self.resultParametricCoordinates.x - 1.0 < self.toleranceIntersection:
                    self.type = Type.IntersectionOnSegment

        else:
            parallelism2 = math.fabs(self.matrixTangentVector.col1.x * self.rightHandSide.y - self.rightHandSide.x * self.matrixTangentVector.col1.y)
            squaredNormFirstEdge = self.matrixTangentVector.col1.squaredNorm()
            check2 = self.toleranceParallelism * self.toleranceParallelism * squaredNormFirstEdge * self.rightHandSide.squaredNorm()

            if parallelism2 * parallelism2 <= check2 :
                tempNorm = 1.0 / squaredNormFirstEdge
                self.resultParametricCoordinates.x = (self.matrixTangentVector.col1.x * self.rightHandSide.x + self.matrixTangentVector.col1.y * self.rightHandSide.y) * tempNorm
                self.resultParametricCoordinates.y = self.resultParametricCoordinates.x - (self.matrixTangentVector.col1.x * self.matrixTangentVector.col2.x + self.matrixTangentVector.col1.y * self.matrixTangentVector.col2.y) * tempNorm
                intersection = True
                self.type = Type.IntersectionParallelOnLine

                if self.resultParametricCoordinates.y < self.resultParametricCoordinates.x:
                    tmp = self.resultParametricCoordinates.x
                    self.resultParametricCoordinates.x = self.resultParametricCoordinates.y
                    self.resultParametricCoordinates.y = tmp

                if (self.resultParametricCoordinates.x > -self.toleranceIntersection and self.resultParametricCoordinates.x - 1.0 < self.toleranceIntersection) or (self.resultParametricCoordinates.y > -self.toleranceIntersection and self.resultParametricCoordinates.y - 1.0 < self.toleranceIntersection):
                    self.type = Type.IntersectionParallelOnSegment
                else:
                    if self.resultParametricCoordinates.x < self.toleranceIntersection and self.resultParametricCoordinates.y - 1.0 > -self.toleranceIntersection:
                        self.type = Type.IntersectionParallelOnSegment
        if self.resultParametricCoordinates.x < -self.toleranceIntersection or self.resultParametricCoordinates.x > 1.0 + self.toleranceIntersection:
            self.positionIntersectionFirstEdge = Position.Outer

        elif (self.resultParametricCoordinates.x > -self.toleranceIntersection) and (self.resultParametricCoordinates.x < self.toleranceIntersection):
            self.resultParametricCoordinates.x = 0.0
            self.positionIntersectionFirstEdge = Position.Begin

        elif (self.resultParametricCoordinates.x > 1.0 - self.toleranceIntersection) and (self.resultParametricCoordinates.x < 1.0 + self.toleranceIntersection):
            self.resultParametricCoordinates.x = 1.0
            self.positionIntersectionFirstEdge = Position.End

        else:
            self.positionIntersectionFirstEdge = Position.Inner

        if self.resultParametricCoordinates.y < -self.toleranceIntersection or self.resultParametricCoordinates.y > 1.0 + self.toleranceIntersection:
            self.positionIntersectionSecondEdge = Position.Outer

        elif (self.resultParametricCoordinates.y > -self.toleranceIntersection) and (self.resultParametricCoordinates.y < self.toleranceIntersection):
            self.resultParametricCoordinates.y = 0.0
            self.positionIntersectionSecondEdge = Position.Begin

        elif (self.resultParametricCoordinates.y > 1.0 - self.toleranceIntersection) and (self.resultParametricCoordinates.y <= 1.0 + self.toleranceIntersection):
            self.resultParametricCoordinates.y = 1.0
            self.positionIntersectionSecondEdge = Position.End

        else:
            self.positionIntersectionSecondEdge = Position.Inner

        return intersection