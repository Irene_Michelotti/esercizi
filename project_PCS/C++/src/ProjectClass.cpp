#include "ProjectClass.hpp"
#include "Intersector1D1D.hpp"

namespace ProgettoNamespace {

Point::Point()
{
    X = 0.0;
    Y = 0.0;
}

Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}

Segment::Segment()
{
    From = Point();
    To = Point();
}

Segment::Segment(const Point &from, const Point &to)
{
    From = Point(from.X, from.Y);
    To = Point(to.X, to.Y);
}

Segment::Segment(const Segment &segment)
{
    From = segment.From;
    To = segment.To;
}

Polygon::Polygon()
{
    Vertices.push_back(Point());
}

Polygon::Polygon(const vector<Point> &vertices)
{
    unsigned int numVertices = vertices.size();
    for (unsigned int i =0; i<numVertices; i++ )
        Vertices.push_back(vertices[i]);
}

vector<Segment> Polygon::createPolygonEdges(const vector<Point>& vertices)
{
    unsigned int NumVertices = vertices.size();
    vector<Segment> Edges;
    Edges.resize(NumVertices);

    for (unsigned int i = 0; i<NumVertices-1; i++)
    {

        Edges[i].From = vertices[i];
        Edges[i].To = vertices[i+1];
    }

    Edges[NumVertices-1].From = vertices[NumVertices-1];
    Edges[NumVertices-1].To = vertices[0];

    return Edges;
}

void PolygonFactory::CutPolygon(const vector<Point>& points, const vector<int>& polygonVertices, const Segment& segment)
{
    vector<Point> orderedPoints;
    unsigned int NumVertices = polygonVertices.size();
    orderedPoints.resize(NumVertices);

    //ordiniamo il vettore di punti del poligono in input secondo
    //l'ordine (antiorario) dato dal vettore polygonVertices
    for(unsigned int i = 0; i<NumVertices; i++)
    {
        int position = polygonVertices[i];
        orderedPoints[i]=points[position];
    }

    Polygon poligono = Polygon(orderedPoints);
    vector<Segment> orderedSegments;
    orderedSegments = poligono.createPolygonEdges(orderedPoints);

    segmentIntersection.resize(NumVertices);
    newPoint.resize(NumVertices);
    for (unsigned int i=0; i<NumVertices; i++)

        //inseriamo i primi NumVertices punti, passati in input
        //per poi aggiungere gli eventuali punti di intersezione
        newPoint[i] = points[i];

    Intersector1D1D intersector;
    Vector2d originSegment(segment.From.X, segment.From.Y);
    Vector2d endSegment(segment.To.X, segment.To.Y);
    intersector.SetFirstSegment(originSegment, endSegment);

    //svuoto il vettore da eventuali elementi, ottengo vettore di lunghezza zero
    parametricCoordinateIntersections.clear();
    orderedIntersectionPoint.clear();
    interToSegment.clear();

    //ciclo su tutti i lati del poligono
    for(unsigned int i = 0; i< NumVertices; i++)
    {
        Vector2d originEdge(orderedSegments[i].From.X, orderedSegments[i].From.Y);
        Vector2d endEdge(orderedSegments[i].To.X, orderedSegments[i].To.Y);
        intersector.SetSecondSegment(originEdge, endEdge);

        //creo variabile che ci dice se c'è o no
        //intersezione tra i due segmenti (se sono o non sono paralleli)
        bool isIntersection =  intersector.ComputeIntersection();

        Intersector1D1D::Position position = intersector.positionIntersectionSecondEdge;
        //se sul secondo segmento, l'intersezione non è esterna allora faccio...
        if (isIntersection == true && (position == 1 || position == 2))
        {
            double tolerance= 1.0E-7;
            if(position == 2) //se è end (se intersezione coincide con vertice finale del lato)
            {
                //segmento dal vertice che è anche punto di intersezione all'inizio del segmento che taglia
                Vector2d vecPQ(segment.From.X- orderedPoints[i+1].X,segment.From.Y- orderedPoints[i+1].Y);
                //segmento dal punto di intersezione
                //al vertice precedente a quello che è anche punto di intersezione
                Vector2d vecPRprec(orderedPoints[i].X-orderedPoints[i+1].X,orderedPoints[i].Y-orderedPoints[i+1].Y);
                //segmento dal punto di intersezione
                //al vertice successivo a quello che è anche punto di intersezione
                Vector2d vecPRsucc(orderedPoints[i+2].X-orderedPoints[i+1].X,orderedPoints[i+2].Y-orderedPoints[i+1].Y);

                double crossPrec = vecPQ(0)*vecPRprec(1)-vecPQ(1)*vecPRprec(0);
                double crossSucc = vecPQ(0)*vecPRsucc(1)-vecPQ(1)*vecPRsucc(0);

                //se il lato precedente e quello successivo sono dalla stessa parte rispetto alla retta secante
                if ((crossPrec < -tolerance && crossSucc < -tolerance) ||(crossPrec > tolerance && crossSucc >tolerance))
                {
                    //aggiungo al vettore delle coordinate parametriche sulla retta
                    //la coordinata parametrica del nuovo punto di intersezione
                    parametricCoordinateIntersections.push_back(intersector.FirstParametricCoordinate());
                    //mi salvo il punto di intersezione come vettore (perchè viene restituito così dalla classe intersector)
                    Vector2d intersectionVector1 = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment);
                    //converto il vettore sopra in punto, come lo vuole il nostro problema
                    Point intersectionPoint1 = Point(intersectionVector1(0), intersectionVector1(1));
                    //aggiungo in nuovo punto al vettore newPoint
                    newPoint.push_back(intersectionPoint1);
                    //aggiungo il punto di intersezione anche a orderedIntersectionPoint
                    orderedIntersectionPoint.push_back(intersectionPoint1);
                    //aggiungo a interToSegment la posizione del nuovo punto di intersezione nell'orderedIntersectionPoint
                    interToSegment.push_back(i);

                    //aggiungo al vettore delle coordinate parametriche sulla retta
                    //la coordinata parametrica del nuovo punto di intersezione
                    parametricCoordinateIntersections.push_back(intersector.FirstParametricCoordinate());
                    //mi salvo il punto di intersezione come vettore (perchè viene restituito così dalla classe intersector)
                    Vector2d intersectionVector2 = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment);
                    //converto il vettore sopra in punto, come lo vuole il nostro problema
                    Point intersectionPoint2 = Point(intersectionVector2(0), intersectionVector2(1));
                    //aggiungo in nuovo punto al vettore newPoint
                    newPoint.push_back(intersectionPoint2);
                    //aggiungo il punto di intersezione anche a orderedIntersectionPoint
                    orderedIntersectionPoint.push_back(intersectionPoint2);
                    //aggiungo a interToSegment la posizione del nuovo punto di intersezione nell'orderedIntersectionPoint
                    interToSegment.push_back(i+1);
                    i++;
                }
                else //se il lato precedente e quello successivo sono dalla stessa parte rispetto alla retta secante
                {
                    //aggiungo al vettore delle coordinate parametriche sulla retta
                    //la coordinata parametrica del nuovo punto di intersezione
                    parametricCoordinateIntersections.push_back(intersector.FirstParametricCoordinate());
                    //mi salvo il punto di intersezione come vettore (perchè viene restituito così dalla classe intersector)
                    Vector2d intersectionVector1 = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment);
                    //converto il vettore sopra in punto, come lo vuole il nostro problema
                    Point intersectionPoint1 = Point(intersectionVector1(0), intersectionVector1(1));
                    //aggiungo in nuovo punto al vettore newPoint
                    newPoint.push_back(intersectionPoint1);
                    //aggiungo il punto di intersezione anche a orderedIntersectionPoint
                    orderedIntersectionPoint.push_back(intersectionPoint1);
                    //aggiungo a interToSegment la posizione del nuovo punto di intersezione nell'orderedIntersectionPoint
                    interToSegment.push_back(i);
                }

            }
            else  //se intersezione è interna al lato
            {

                //aggiungo al vettore delle coordinate parametriche sulla retta
                //la coordinata parametrica del nuovo punto di intersezione
                parametricCoordinateIntersections.push_back(intersector.FirstParametricCoordinate());
                //mi salvo il punto di intersezione come vettore (perchè viene restituito così dalla classe intersector)
                Vector2d intersectionVector = intersector.IntersectionFirstParametricCoordinate(originSegment, endSegment);
                //converto il vettore sopra in punto, come lo vuole il nostro problema
                Point intersectionPoint = Point(intersectionVector(0), intersectionVector(1));
                //aggiungo in nuovo punto al vettore newPoint
                newPoint.push_back(intersectionPoint);
                //aggiungo il punto di intersezione anche a orderedIntersectionPoint
                orderedIntersectionPoint.push_back(intersectionPoint);
                //aggiungo a interToSegment la posizione del nuovo punto di intersezione nell'orderedIntersectionPoint
                interToSegment.push_back(i);
            }

            unsigned  int n = parametricCoordinateIntersections.size(); //numero di punti di intersezione
            //ordino i vettori parametricCoordinateIntersections, orderedIntersectionPoint, interToSegment
            //e newPoint (quest'ultimo dalla posizione NumVertices in poi (ovvero solo i punti di intersezione e non i vertici del poligono))
            //in base alla coordinata parametrica in ordine crescente
            for (unsigned int j =0; j<n; j++)
            {
                if(parametricCoordinateIntersections[n-1]<=parametricCoordinateIntersections[j])
                {
                    double tempCoord = parametricCoordinateIntersections[n-1];
                    Point tempPoint = orderedIntersectionPoint[n-1];
                    int tempIntSeg = interToSegment[n-1];
                    Point tempNewPoint = newPoint[NumVertices+n-1];
                    for(unsigned int k = n-1; k>j ; k--)
                    {
                        parametricCoordinateIntersections[k] = parametricCoordinateIntersections[k-1];
                        orderedIntersectionPoint[k] = orderedIntersectionPoint[k-1];
                        interToSegment[k] = interToSegment[k-1];
                        newPoint[NumVertices+k] = newPoint[NumVertices+k-1];
                    }

                    parametricCoordinateIntersections[j] = tempCoord;
                    orderedIntersectionPoint[j]= tempPoint;
                    interToSegment[j] = tempIntSeg;
                    newPoint[NumVertices+j] = tempNewPoint;
                }
            }
        }

        else  //se non c'è intersezione sul lato del poligono
            segmentIntersection[i] = -1;
    }

    //assegno a segmentIntersection gli id del punti di intersezione ordinati secondo la coordinata parametrica
    for(unsigned int i = 0; i<NumVertices; i++)
    {
        if(segmentIntersection[i] != -1)
        {
            for(unsigned int j = 0; j<interToSegment.size(); j++)
            {
                if(interToSegment[j] == i)
                    segmentIntersection[i] = j;
            }
        }
    }
}

void ProgettoNamespace::PolygonFactory::CreatePolygons(const vector<int> &polygonVertices)
{
    cuttedPolygons.clear(); //svuoto il vettore da eventuali elementi, ottengo vettore di lunghezza zero

    //inizializzo a false il vettore visitedEdge
    visitedEdge.resize(polygonVertices.size());
    for (unsigned int i = 0; i<polygonVertices.size();i++)
        visitedEdge[i]=false;


    unsigned int numberVertices = polygonVertices.size();
    //ciclo su tutti i vertici del poligono
    for(unsigned int i=0; i<numberVertices; i++)
    {

        //se il vertice i-esimo non è stato visitato
        if(visitedEdge[i]==false)
        {
            unsigned int j = i;
            vector<int> newPolygon;
            newPolygon.clear();
            visitedEdge[i]=true;
            newPolygon.push_back(polygonVertices[i]);

            //ciclo fino a che non torno al vertice di partenza
            bool flag = false;
            while(polygonVertices[(j+1)%numberVertices] != polygonVertices[i] && flag == false)  //j+1 modulo 12 per tornare a 0 una volta arrivati a 12
            {
                //se c'è intersezione sul lato j-esimo
                if(segmentIntersection[j]!=-1 )
                {
                    //se il punto di intersezione ha id pari
                    if(segmentIntersection[j]%2 == 0)
                    {
                        int interPointId = numberVertices + segmentIntersection[j]; //id del punto di intersezione in base al vettore newPoint
                        newPolygon.push_back(interPointId); //aggiungo l'id del punto di intersezione a newPolygon
                        newPolygon.push_back(interPointId + 1); //aggiungo l'id successivo a newPolygon per rimanere dentro
                        //il poligono (essendo l'id del pto di intersezione pari)
                        j = 1 + interToSegment[segmentIntersection[j]+1]; //è l'id del vertice del poligono successivo al punto di intersezione
                        if (polygonVertices[j%numberVertices]!=polygonVertices[i])
                        {
                            visitedEdge[j%numberVertices]=true;
                            newPolygon.push_back(polygonVertices[j%numberVertices]);

                        }
                        else
                            flag = true;
                    }
                    else  //se il punto di intersezione ha id dispari
                    {
                        int interPointId = polygonVertices.size() + segmentIntersection[j]; //id del punto di intersezione in base al vettore newPoint
                        newPolygon.push_back(interPointId); //aggiungo l'id del punto di intersezione a newPolygon
                        newPolygon.push_back(interPointId - 1); //aggiungo l'id precedente a newPolygon per rimanere dentro
                        //il poligono (essendo l'id del pto di intersezione dispari)
                        j = 1+ interToSegment[segmentIntersection[j]-1]; //è l'id del vertice del poligono successivo al punto di intersezione
                        if (polygonVertices[j%numberVertices]!=polygonVertices[i])
                        {
                            visitedEdge[j%numberVertices]=true;
                            newPolygon.push_back(polygonVertices[j%numberVertices]);
                        }
                        else
                            flag = true;
                    }
                }
                else //se non c'è intersezione sul lato j-esimo del poligono
                {
                    visitedEdge[(j+1)%numberVertices]=true;
                    newPolygon.push_back(polygonVertices[(j+1)%numberVertices]);

                    j++;
                }

            }
            cuttedPolygons.push_back(newPolygon);
        }
    }
}
}
