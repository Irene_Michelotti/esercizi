#ifndef PROGETTO_H
#define PROGETTO_H

#include "iostream"
#include "Eigen"
#include <vector>
#include <math.h>
#include "Intersector1D1D.hpp"
using namespace std;
using namespace Eigen;

namespace ProgettoNamespace {

class Point {
public:
    double X;
    double Y;

    Point();
    Point(const double& x, const double& y);
    Point(const Point& point);
};

class Segment {
public:
    Point From;
    Point To;

    Segment();
    Segment(const Point& from, const Point& to);
    Segment(const Segment& segment);
};

class Polygon {
public:
    vector<Point> Vertices;

    Polygon();
    Polygon(const vector<Point>& vertices);

    vector<Segment> createPolygonEdges(const vector<Point>& vertices);
};

class IPolygonFactory
{
public:
    virtual void CutPolygon(const vector<Point>& points, const vector<int>& polygonVertices, const Segment& segment) = 0;
    virtual void CreatePolygons(const vector<int>& polygonVertices) = 0;
};



class PolygonFactory : public IPolygonFactory
{
public:
    vector<Point> newPoint;
    vector<int> segmentIntersection;        //posizione i-esima corrisponde id del punto di intersezione sul segmento i-esimo,
                                            //se presente, altrimenti -1
    vector<double> parametricCoordinateIntersections;   //coordinate parametriche delle intersezioni sulla retta/segmento
    vector<Point> orderedIntersectionPoint; //punti di intersezioni ordinati secondo l'ascissa curvilinea in modo crescente
    vector<int> interToSegment;             //posizione i-esima corrisponde id del segmento che ospita il punto di intersezione i-esimo


   void CutPolygon(const vector<Point>& points, const vector<int>& polygonVertices, const Segment& segment);

   vector<vector<int>> cuttedPolygons;  //indici dei vertici dei poligoni ottenuti dal taglio in senso antiorario
   vector<bool> visitedEdge;
   void CreatePolygons(const vector<int>& polygonVertices);

};

}

#endif // PROGETTO_H
