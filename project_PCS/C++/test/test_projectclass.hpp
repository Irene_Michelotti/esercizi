#ifndef __TEST_PROJECTCLASS_H
#define __TEST_PROJECTCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "ProjectClass.hpp"

using namespace ProgettoNamespace;
using namespace testing;
using namespace std;

namespace ProjectTesting {

TEST(TestPolygon, TestCreatePolygonEdges )
{
    vector<Point> vertices;
    vertices.clear();
    vertices.push_back(Point(0.0,0.0));
    vertices.push_back(Point(2.0,0.0));
    vertices.push_back(Point(2.0,3.0));

    ProgettoNamespace::Polygon polygonNew = Polygon(vertices);

    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[0].From.X, 0.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[0].From.Y, 0.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[0].To.X, 2.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[0].To.Y, 0.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[1].From.X, 2.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[1].From.Y, 0.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[1].To.X, 2.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[1].To.Y, 3.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[2].From.X, 2.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[2].From.Y, 3.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[2].To.X, 0.0);
    EXPECT_EQ(polygonNew.createPolygonEdges(vertices)[2].To.Y, 0.0);
}


TEST(TestPolygonFactory, TestCutPolygon1 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(5.0,3.1));
    points.push_back(Point(1.0,1.0));
    points.push_back(Point(1.0,3.1));
    points.push_back(Point(5.0,1.0));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(1);
    polygonVertices.push_back(3);
    polygonVertices.push_back(0);
    polygonVertices.push_back(2);

    Point from = Point(2.0,1.2);
    Point to = Point(4.0,3.0);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[4].X,1.777777777777777781);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y, 1);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].X,4.1111111111111107);
    EXPECT_EQ(newPolygonFactory.newPoint[5].Y, 3.1);
}

TEST(TestPolygonFactory, TestCreatePolygon1 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(5.0,3.1));
    points.push_back(Point(1.0,1.0));
    points.push_back(Point(1.0,3.1));
    points.push_back(Point(5.0,1.0));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(1);
    polygonVertices.push_back(3);
    polygonVertices.push_back(0);
    polygonVertices.push_back(2);

    Point from = Point(2.0,1.2);
    Point to = Point(4.0,3.0);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);

    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(1,4,5,2));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(3,0,5,4));
}

TEST(TestPolygonFactory, TestCutPolygon2 )
{

    vector<Point> points;
    points.clear();
    points.push_back(Point(1.6,4.2));
    points.push_back(Point(3.4,4.2));
    points.push_back(Point(1.0,2.1));
    points.push_back(Point(2.5,1.0));
    points.push_back(Point(4.0,2.1));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);
    polygonVertices.push_back(2);

    Point from = Point(1.4,2.75);
    Point to = Point(3.6,2.2);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[4].X,points[4].X);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y,points[4].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].X,1.2);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].Y,2.8);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].X,4.0);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].Y,2.1);

}

TEST(TestPolygonFactory, TestCreatePolygon2 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(1.6,4.2));
    points.push_back(Point(3.4,4.2));
    points.push_back(Point(1.0,2.1));
    points.push_back(Point(2.5,1.0));
    points.push_back(Point(4.0,2.1));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);
    polygonVertices.push_back(2);

    Point from = Point(1.4,2.75);
    Point to = Point(3.6,2.2);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);

    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(3,6,5,2));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(4,1,0,5,6));
}


TEST(TestPolygonFactory, TestCutPolygon3 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(1.0,4.0));
    points.push_back(Point(5.5,4.8));
    points.push_back(Point(3.2,4.2));
    points.push_back(Point(4.0,6.2));
    points.push_back(Point(1.5,1.0));
    points.push_back(Point(5.6,1.5));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(4);
    polygonVertices.push_back(5);
    polygonVertices.push_back(1);
    polygonVertices.push_back(3);
    polygonVertices.push_back(2);
    polygonVertices.push_back(0);

    Point from = Point(2.0,3.7);
    Point to = Point(4.1,5.9);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[4].X,points[4].X);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y,points[4].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[5].X,points[5].X);
    EXPECT_EQ(newPolygonFactory.newPoint[5].Y,points[5].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].X,1.1912162162162161);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].Y, 2.8527027027027021);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[7].X,2.4085972850678732);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[7].Y, 4.1280542986425335);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[8].X,3.7213114754098369);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[8].Y, 5.503278688524591);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[9].X,4.2043269230769225);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[9].Y, 6.0092948717948724);
}

TEST(TestPolygonFactory, TestCreatePolygon3 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(1.0,4.0));
    points.push_back(Point(5.5,4.8));
    points.push_back(Point(3.2,4.2));
    points.push_back(Point(4.0,6.2));
    points.push_back(Point(1.5,1.0));
    points.push_back(Point(5.6,1.5));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(4);
    polygonVertices.push_back(5);
    polygonVertices.push_back(1);
    polygonVertices.push_back(3);
    polygonVertices.push_back(2);
    polygonVertices.push_back(0);

    Point from = Point(2.0,3.7);
    Point to = Point(4.1,5.9);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);

    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(4,5,1,9,8,2,7,6));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(3,8,9));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[2], ElementsAre(0,6,7));
}

TEST(TestPolygonFactory, TestCutPolygon4 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(5.38,5.77));
    points.push_back(Point(9.92,3.8));
    points.push_back(Point(2.0,3.0));
    points.push_back(Point(5.19,1.47));
    points.push_back(Point(9.8,1.48));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(2);
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);

    Point from = Point(3.66,6.01);
    Point to = Point(6.45,2.72);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[4].X,points[4].X);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y,points[4].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].X,7.5057704609471658);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].Y, 1.4750233632558509);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].X,4.48531363238454);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].Y, 5.0367806987293422);
}

TEST(TestPolygonFactory, TestCreatePolygon4 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(5.38,5.77));
    points.push_back(Point(9.92,3.8));
    points.push_back(Point(2.0,3.0));
    points.push_back(Point(5.19,1.47));
    points.push_back(Point(9.8,1.48));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(2);
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);

    Point from = Point(3.66,6.01);
    Point to = Point(6.45,2.72);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);

    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(2,3,6,5));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(4,1,0,5,6));
}



TEST(TestPolygonFactory, TestCutPolygon5 )
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(4.0,6.32));
    points.push_back(Point(5.98,4.58));
    points.push_back(Point(1.86,4.56));
    points.push_back(Point(2.61,5.4));
    points.push_back(Point(3.23,1.78));
    points.push_back(Point(7.34,2.02));
    points.push_back(Point(8.45,4.64));
    points.push_back(Point(6.93,6.38));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(4);
    polygonVertices.push_back(5);
    polygonVertices.push_back(6);
    polygonVertices.push_back(7);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);
    polygonVertices.push_back(3);
    polygonVertices.push_back(2);


    Point from = Point(1.4,5.18);
    Point to = Point(8.78,5.46);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[4].X,points[4].X);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y,points[4].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[5].X,points[5].X);
    EXPECT_EQ(newPolygonFactory.newPoint[5].Y,points[5].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[6].X,points[6].X);
    EXPECT_EQ(newPolygonFactory.newPoint[6].Y,points[6].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[7].X,points[7].X);
    EXPECT_EQ(newPolygonFactory.newPoint[7].Y,points[7].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[8].X,2.4491103987176914);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[8].Y,5.2198036465638147);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[9].X, 5.1359473259876376);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[9].Y,5.3217432589805611);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[10].X,6.3967214625902589);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[10].Y,5.3695775080657553);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[11].X,7.7672442488015179);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[11].Y,5.4215756625561546);
}

TEST(TestPolygonFactory, TestCreatePolygon5)
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(4.0,6.32));
    points.push_back(Point(5.98,4.58));
    points.push_back(Point(1.86,4.56));
    points.push_back(Point(2.61,5.4));
    points.push_back(Point(3.23,1.78));
    points.push_back(Point(7.34,2.02));
    points.push_back(Point(8.45,4.64));
    points.push_back(Point(6.93,6.38));

    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(4);
    polygonVertices.push_back(5);
    polygonVertices.push_back(6);
    polygonVertices.push_back(7);
    polygonVertices.push_back(1);
    polygonVertices.push_back(0);
    polygonVertices.push_back(3);
    polygonVertices.push_back(2);


    Point from = Point(1.4,5.18);
    Point to = Point(8.78,5.46);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);

    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(4,5,6,11,10,1,9,8,2));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(7,10,11));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[2], ElementsAre(0,3,8,9));
}

TEST(TestPolygonFactory, TestCutPolygon6)
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(1.85,1.08));
    points.push_back(Point(4.64,1.07));
    points.push_back(Point(5.41,3.24));
    points.push_back(Point(3.78,2.60));
    points.push_back(Point(2.41,4.16));


    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(0);
    polygonVertices.push_back(1);
    polygonVertices.push_back(2);
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);

    Point from = Point(1.71,3.74);
    Point to = Point(5.41,3.24);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);

    EXPECT_EQ(newPolygonFactory.newPoint[0].X,points[0].X);
    EXPECT_EQ(newPolygonFactory.newPoint[0].Y,points[0].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[1].X,points[1].X);
    EXPECT_EQ(newPolygonFactory.newPoint[1].Y,points[1].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[2].X,points[2].X);
    EXPECT_EQ(newPolygonFactory.newPoint[2].Y,points[2].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[3].X,points[3].X);
    EXPECT_EQ(newPolygonFactory.newPoint[3].Y,points[3].Y);
    EXPECT_EQ(newPolygonFactory.newPoint[4].X,points[4].X);
    EXPECT_EQ(newPolygonFactory.newPoint[4].Y,points[4].Y);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].X,2.3186810551558756);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[5].Y,3.6577458033573143);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].X,2.9227737369766071);
    EXPECT_DOUBLE_EQ(newPolygonFactory.newPoint[6].Y,3.5761116571653231);
    EXPECT_EQ(newPolygonFactory.newPoint[7].X,5.41);
    EXPECT_EQ(newPolygonFactory.newPoint[7].Y,3.24);
    EXPECT_EQ(newPolygonFactory.newPoint[8].X,5.41);
    EXPECT_EQ(newPolygonFactory.newPoint[8].Y,3.24);

}

TEST(TestPolygonFactory, TestCreatePolygon6)
{
    vector<Point> points;
    points.clear();
    points.push_back(Point(1.85,1.08));
    points.push_back(Point(4.64,1.07));
    points.push_back(Point(5.41,3.24));
    points.push_back(Point(3.78,2.60));
    points.push_back(Point(2.41,4.16));


    vector<int> polygonVertices;
    polygonVertices.clear();
    polygonVertices.push_back(0);
    polygonVertices.push_back(1);
    polygonVertices.push_back(2);
    polygonVertices.push_back(3);
    polygonVertices.push_back(4);

    Point from = Point(1.71,3.74);
    Point to = Point(5.41,3.24);
    Segment segment = Segment(from,to);

    ProgettoNamespace::PolygonFactory newPolygonFactory;
    newPolygonFactory.CutPolygon(points,polygonVertices,segment);
    newPolygonFactory.CreatePolygons(polygonVertices);


    ASSERT_THAT(newPolygonFactory.cuttedPolygons[0], ElementsAre(0,1,8,7,3,6,5));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[1], ElementsAre(2,7,8));
    ASSERT_THAT(newPolygonFactory.cuttedPolygons[2], ElementsAre(4,5,6));

}

}

#endif // __TEST_PROJECTCLASS_H
