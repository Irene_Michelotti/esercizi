#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

//costruttore della classe Point
Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

//costruttore vuoto, inizializza a (0.0,0.0) il punto
Point::Point()
{
    X = 0.0;
    Y = 0.0;
}

//costruttore copia
Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}



Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Area() const
{
    return M_PI*_a*_b;
}

//costruttore di Circle eredita costruttore di Ellipse
Circle::Circle(const Point &center, const int &radius) : Ellipse(center,radius,radius) {}



Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
}

double Triangle::Area() const
{
    return abs(_p1.X*(_p2.Y-_p3.Y) + _p2.X*(_p3.Y-_p1.Y) + _p3.X*(_p1.Y-_p2.Y))/2;
}

//costruttore di TriangleEquilateral eredita costruttore di Triangle
TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(p1, Point(p1.X+edge, p1.Y), Point(p1.X+edge/2, p1.Y+sqrt(3)/2*edge)) {}



Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
}

double Quadrilateral::Area() const
{
    return 0.5*((_p1.X*_p2.Y + _p2.X*_p3.Y + _p3.X*_p4.Y + _p4.X*_p1.Y) - (_p2.X*_p1.Y + _p3.X*_p2.Y + _p4.X*_p3.Y + _p1.X*_p4.Y));
}

//costruttore di Parallelogram eredita costruttore di Quadrilateral
Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1, p2, Point(p2.X+p4.X-p1.X, p2.Y+p4.Y-p1.Y), p4) {}

//costruttore di Rectangle eredita costruttore di Parallelogram
Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1, Point(p1.X+base, p1.Y), Point(p1.X, p1.Y+height)) {}

//costruttore di Square eredita costruttore di Rectangle
Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge) {}


}
