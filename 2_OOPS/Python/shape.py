import math

class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self) -> float:
        return math.pi*self._a*self._b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)   #ereditarietà


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
    def area(self) -> float:
        x1 = self._p1.X
        y1 = self._p1.Y
        x2 = self._p2.X
        y2 = self._p2.Y
        x3 = self._p3.X
        y3 = self._p3.Y
        return abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, Point(p1.X + edge, p1.Y),Point(p1.X + edge / 2, p1.Y + math.sqrt(3) / 2 * edge))  #ereditarietà da Triangle


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4
    def area(self) -> float:
        x1 = self._p1.X
        y1 = self._p1.Y
        x2 = self._p2.X
        y2 = self._p2.Y
        x3 = self._p3.X
        y3 = self._p3.Y
        x4 = self._p4.X
        y4 = self._p4.Y

        return 0.5 * ((x1 * y2 + x2 * y3 + x3 * y4 + x4 * y1) - (x2 * y1 + x3 * y2 + x4 * y3 + x1 * y4))



class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, Point(p4.X + p2.X - p1.X, p2.Y+p4.Y-p1.Y), p4)  #ereditarietà da Quadrilateral


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__(p1, Point(p1.X+base, p1.Y), Point(p1.X, p1.Y+height))  #ereditarietà da Parallelogram


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)   #ereditarietà da Rectangle
