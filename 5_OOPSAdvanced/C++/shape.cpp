#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

//costruttore vuoto: inizializza un punto a (0.0, 0.0)
Point::Point()
{
    X=0.0;
    Y=0.0;
}

//costruttore della classe Point
Point::Point(const double &x, const double &y)
{
    X=x;
    Y=y;
}

//costruttore di copia
Point::Point(const Point &point)
{
    X=point.X;
    Y=point.Y;
}

//calcola la norma euclidea
double Point::ComputeNorm2() const
{
    return sqrt(pow(X,2)+pow(Y,2));
}

//overloading degli operatori:

Point Point::operator+(const Point& point) const
{
    return Point(X + point.X, Y + point.Y);
}

Point Point::operator-(const Point& point) const
{
    return Point(X - point.X, Y - point.Y);
}

Point&Point::operator-=(const Point& point)
{
    X -= point.X;
    Y -= point.Y;
    return *this;
}

Point&Point::operator+=(const Point& point)
{
    X += point.X;
    Y += point.Y;
    return *this;
}


//costruttore vuoto: inizializza l'ellisse a un'ellisse degenere ridotta al solo punto (0.0, 0.0)
Ellipse::Ellipse()
{
    _center = Point();
    _a = 0.0;
    _b = 0.0;
}

//costruttore di un'ellisse, dove center=centro dell'ellisse, a=semiasse orizzontale dell'ellisse, b=semiasse verticale dell'ellisse
Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center = center;
    _a = a;
    _b = b;
}

//aggiunge il centro dell'ellisse
void Ellipse::AddVertex(const Point &point)
{
    _center = point;
}

//imposta il semiasse orizzontale dell'ellisse
void Ellipse::SetSemiAxisA(const double &a)
{
    _a = a;
}

//imposta il semiasse verticale dell'ellisse
void Ellipse::SetSemiAxisB(const double &b)
{
    _b = b;
}

//calcola il perimetro dell'ellisse
double Ellipse::Perimeter() const
  {
    return 2*M_PI*sqrt(((_a*_a)+(_b*_b))/2);
  }

//costruttore vuoto: inizializza il triangolo a un triangolo degenere ridotto al solo punto (0.0, 0.0)
Triangle::Triangle()
{
    points.push_back(Point());
}

//costruttore di un triangolo, dove p1,p2,p3=vertici del triangolo
Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
{
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
}

//aggiunge un vertice del triangolo
void Triangle::AddVertex(const Point &point)
{
    points.push_back(point);
}

//calcola il perimetro di un triangolo
 double Triangle::Perimeter() const
 {
    double perimeter = 0;
    double l1 = (points[1]-points[0]).ComputeNorm2();
    double l2 = (points[2]-points[1]).ComputeNorm2();
    double l3 = (points[2]-points[0]).ComputeNorm2();
    perimeter= l1 + l2 + l3;

    return perimeter;
 }

//costruttore del triangolo equilatero (eredita costruttore del triangolo)
TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                         const double& edge) : Triangle(p1, Point(p1.X+edge, p1.Y), Point(p1.X+edge/2, p1.Y+sqrt(3)/2*edge))
{ }


//costruttore vuoto: inizializza il quadrilatero a un quadrilatero degenere ridotto al solo punto (0.0, 0.0)
Quadrilateral::Quadrilateral()
{
    points.push_back(Point());
}

//costruttore di un quadrilatero, dove p1,p2,p3,p4=vertici del quadrilatero
Quadrilateral::Quadrilateral(const Point& p1,
                             const Point& p2,
                             const Point& p3,
                             const Point& p4)
{
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);
}

//aggiunge un vertice del quadrilatero
void Quadrilateral::AddVertex(const Point &p)
{
    points.push_back(p);
}

//calcola il perimetro di un quadrilatero
double Quadrilateral::Perimeter() const
{
    double perimeter = 0;
    double l1 = (points[1]-points[0]).ComputeNorm2();
    double l2 = (points[2]-points[1]).ComputeNorm2();
    double l3 = (points[3]-points[2]).ComputeNorm2();
    double l4 = (points[0]-points[3]).ComputeNorm2();
    perimeter= l1 + l2 + l3 + l4;
    return perimeter;
}

//costruttore di un rettangolo (eredita costruttore del quadrilatero)
Rectangle::Rectangle(const Point& p1,
                     const double& base,
                     const double& height) : Quadrilateral(p1, Point(p1.X + base, p1.Y), Point(p1.X + base, p1.Y + height), Point(p1.X, p1.Y + height))
{ }


}
