#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point();                     //costruttore che inizializza un punto a (0.0,0.0)
      Point(const double& x,
            const double& y);     //costruttore
      Point(const Point& point);  //costruttore di copia

      double ComputeNorm2() const;

      Point operator+(const Point& point) const;    //overloading operatori
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " << point.X <<" y = " << point.Y << endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter()<rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter()>rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter()<=rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter()>=rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse();
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { cout<< "Sono nel distruttore di Ellipse"<<endl; }
      void AddVertex(const Point& point);
      void SetSemiAxisA(const double&a);
      void SetSemiAxisB(const double&b);
      double Perimeter() const;
  };

  class Circle : public Ellipse //ereditarietà: la classe Circle è un caso particolare della classe Ellipse
  {
    public:
      Circle() : Ellipse() {}
      Circle(const Point& center,
             const double& radius) : Ellipse(center, radius, radius) { }
      virtual ~Circle() { cout<< "Sono nel distruttore di Circle"<<endl; }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;  //vertici del triangolo

    public:
      Triangle();
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);
      virtual ~Triangle() { cout<< "Sono nel distruttore di Triangle"<<endl; }
      void AddVertex(const Point& point);
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle //ereditarietà: la classe TriangleEquilateral è un caso particolare della classe Triangle
  {
    public:
      TriangleEquilateral() : Triangle() { }
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      virtual ~TriangleEquilateral(){ cout<< "Sono nel distruttore di TriangleEquilateral"<<endl; }
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;   //vertici del quadrilatero

    public:
      Quadrilateral();
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { cout<< "Sono nel distruttore di Quadrilateral"<<endl; }
      void AddVertex(const Point& p);
      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral //ereditarietà: la classe Rectangle è un caso particolare della classe Quadrilateral
  {
    public:
      Rectangle() : Quadrilateral() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) : Quadrilateral(p1, p2, p3, p4) { }
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { cout<< "Sono nel distruttore di Rectangle"<<endl; }
  };

  class Square: public Rectangle //ereditarietà: la classe Square è un caso particolare della classe Rectangle
  {
    public:
      Square() : Rectangle() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) : Rectangle(p1, p2, p3, p4) { }
      Square(const Point& p1,
             const double& edge) : Rectangle(p1, edge, edge){ }
      virtual ~Square() { cout<< "Sono nel distruttore di Square"<<endl; }
  };
}

#endif // SHAPE_H
