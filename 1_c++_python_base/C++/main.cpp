#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    ifstream file;
    file.open(inputFilePath);

    //controlla che file sia stato aperto correttamente
    if (file.fail())
     {
       cerr<< "file open failed"<< endl;
       return false;
     }

    //salva in text la riga del file
    getline(file, text);

    //chiude il file
    file.close();

    return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    unsigned int sizeText;
    sizeText = text.length();  //lunghezza messaggio da criptare
    unsigned int sizePassword;
    sizePassword = password.length();  //lunghezza password


    //controlla che password sia maiuscola
    for(unsigned int k = 0; k < sizePassword; k++)
    {
        if(int(password[k])<65 || int(password[k])>90)
        {
            cout<< "Password is not correct"<< endl;
            return -1;
        }
    }

    encryptedText.resize(sizeText);

    unsigned int j=0;

    //cripta il testo dato in input
    for(unsigned int i=0; i<sizeText; i++)
    {
        encryptedText[i]=text[i]+password[j];
        j++;
        if(j==sizePassword)
            j=0;
    }

    return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    unsigned int sizeText;
    sizeText = text.length();  //lunghezza messaggio da decriptare
    unsigned int sizePassword;
    sizePassword = password.length();  //lunghezza password

    decryptedText.resize(text.length());

    unsigned int j=0;

    //decripta il testo dato in input
    for(unsigned int i=0; i<sizeText; i++)
    {
        decryptedText[i]=text[i]-password[j];
        j++;
        if(j==sizePassword)
            j=0;
    }


  return  true;
}
